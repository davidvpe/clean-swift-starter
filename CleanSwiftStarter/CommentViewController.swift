//
//  CommentViewController.swift
//  CleanSwiftStarter
//
//  Created by David on 6/20/18.
//  Copyright © 2018 David Velarde. All rights reserved.
//

import UIKit

class CommentViewController: UIViewController {

    var arrayComments = [Comment]()
    var postId: String = "0"
    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        NetworkManager.shared.getCommentsForPost(withPostId: postId) { arrayComments in
            self.arrayComments = arrayComments
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}

extension CommentViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayComments.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let comment = arrayComments[indexPath.row]
        if let cell = tableView.dequeueReusableCell(withIdentifier: "CommentCell") as? CommentCell {
            cell.nameLabel.text = comment.name
            cell.emailLabel.text = comment.email
            cell.commentLabel.text = comment.body
            return cell
        }
        return UITableViewCell()
    }
}
